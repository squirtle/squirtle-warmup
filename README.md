# SQuirtLe-WarmUp

This project allows a user to search through a database for various pokemon, moves and their attributes.  Users 
search through the database using a command line query interface.

###Queries should be entered query in the following format:

| What User wants | Name of known column | “Identifier” | (Optional) Name of known column | “Identifier” | etc...

Ensure that each term in the query is separated by a space, or in quotation marks if the command contains multiple words

###Terms:
1. What user wants refers to the target data column
2. Name of known column refers to the column you have an identifier for
3. Identifier is a specific piece of data for the column you specified
4. Optionally, you can add additional columns and identifiers for more specific queries, however this must be done in pairs

###Additional Rules:
1. All column names and identifiers must be from the same table, with the exception of the target column.
2. To make a query use both tables, the target column should be from a different table than the rest of the columns and identifiers.
3. Each column specified in the query must be unique/cannot be specified more than once.
4. This includes the target column, as being able to specify it would make the query pointless.
5. Make sure your query statement does not have any additional spaces, as this will return an error for format.
6. To use the how many command, you are limited to either "pokemon" or "move" which are the table names.

###Examples:
1. | Pokemon move \"Pound\" | returns the pokemon with move pound
2. | Move Accuracy \"100\" Type \"Normal\" Category \"Physical\" | returns the move(s) with accuracy 100, type normal, and category physical
3. | Accuracy Pokemon \"Squirtle\" | returns the accuracy of the move of the pokemon named squirtle, this query uses both tables"
4. | How many \"pokemon\" | returns the total amount of pokemon, can also ask for amount of "move"s

Type "help" to see a help menu

Type "quit" to exit the program