# Code base for project

"""
What they want, name of known column, identifier(has to have quotations)



"""
import pandas as pd

# Column names
pokemon_columns = ["pokemon", "ptype", "attack", "defence", "speed", "move"]
move_columns = ["move", "mtype", "category", "power", "accuracy"]

# Instruction for User


# Load data
#Boolean return value

# Gather user input

# Parsing
def parser(input_string):

    # Separate the data
    query_list = sep_string(input_string.lower())

    # Check for how many function
    if len(query_list) == 3 and query_list[0] == "how" and query_list[1] == "many":
        # Call how many function
        1+1


    #Start forming select_var and where_var etc.
    select_var = query_list[0]
    where_var = []
    identifier = []
    for item in query_list[1:]:
        if(query_list[item] in pokemon_columns):
            where_var.append(query_list[item])
        elif(query_list[item] in move_columns):
            where_var.append(query_list[item])
        elif((query_list[item] not in pokemon_columns) or (query_list[item] not in move_columns)):
            identifier.append(query_list[item])






    return query_list


def sep_string(in_string):

    queery_list = []
    command = ""
    quoted_command = False
    ignor_space = False
    for ch in in_string:

        #if the ch is a space, end of command reached (unless within quotes), a$
        if ch == " " and not quoted_command:
            #if space following quote, it should be ignored
            if ignor_space:
                ignor_space = False
            else:
                queery_list.append(command)
                command = ""

        #if ch is a quote, either start or end of command
        elif ch == '"':
            #if end of command, add command to queery list
            if quoted_command:
                quoted_command = False
                queery_list.append(command)
                command = ""
                ignor_space = True

            #if start of command, set flag so that function will know if quote $
            else:
                quoted_command = True
        else:
            command += ch
    #at this point function adds command if last command is in quotes
    #if ignor space variable is false, the last command still needs to be addesd
    if not ignor_space:
        queery_list.append(command)
    return queery_list


# Translate to SQL query
def database_search(selct_var,where_var,identifier, has_foreign, pokemon_df, move_df):
    print(selct_var)
    print(where_var)
    print(identifier)
    print(has_foreign)

    print("Inside database")


def main():
    print("QUERY STUFF\n")
    print("If you want to quit the program type: quit\n")

    load_input = input("Please type 'load data' to start: ")
    while(load_input != 'load data'):
        print("Sorry you must type load data to start the program\n")
        load_input = input("Please type 'load data' to start: ")

    pokemon_df, move_df = load_data()
    query_input = input("Please type your first query: ")
    while(query_input != "quit"):
     #   select_var, where_var, identifier, has_foreign = parser(query_input)
        query_list =  parser(query_input)
        print(query_list)
        query_input = input("Please type your query: ")
     #   database_search(select_var, where_var, identifier, has_foreign,pokemon_df, move_df)

def load_data():
    # Add exception handling
    pokemon_csv = "pokemon-data.csv"
    move_csv = "move-data.csv"
    pokemon_df = pd.read_csv('../squirtle-warmup/' + pokemon_csv)
    move_df = pd.read_csv('../squirtle-warmup/' + move_csv)
    return pokemon_df, move_df

main()

