# Code base for project

"""
What they want, name of known column, identifier(has to have quotations)



"""
import pandas as pd
from pathlib import Path
import sqlite3
# Column names
pokemon_columns = ["pokemon", "ptype", "attack", "defense", "speed", "move"]
move_columns = ["move", "mtype", "category", "power", "accuracy"]


# Help function
def print_help():
    print("\nEnter your query in the following format:")
    print("| What User wants | Name of known column | “Identifier” | (Optional) Name of known column | “Identifier” | etc...")
    print("Ensure that each term in the query is separated by a space, or in quotation marks if the command contains multiple words")
    print("1. What user wants refers to the target data column")
    print("2. Name of known column refers to the column you have an identifier for")
    print("3. Identifier is a specific piece of data for the column you specified")
    print("4. Optionally, you can add additional columns and identifiers for more specific queries, however this must be done in pairs")
    print("\nAdditional Rules:")
    print("1. All column names and identifiers must be from the same table, with the exception of the target column.")
    print("2. To make a query use both tables, the target column should be from a different table than the rest of the columns and identifiers.")
    print("3. Each column specified in the query must be unique/cannot be specified more than once.")
    print("4. This includes the target column, as being able to specify it would make the query pointless.")
    print("5. Make sure your query statement does not have any additional spaces, as this will return an error for format.")
    print("6. To use the how many command, you are limited to either \"pokemon\" or \"move\" which are the table names.")
    print("\nExamples:")
    print("1. | Pokemon move \"Pound\" | returns the pokemon with move pound")
    print("2. | Move Accuracy \"100\" Type \"Normal\" Category \"Physical\" | returns the move(s) with accuracy 100, type normal, and category physical")
    print("3. | Accuracy Pokemon \"Squirtle\" | returns the accuracy of the move of the pokemon named squirtle, this query uses both tables\n")
    print("4. | How many \"pokemon\" | returns the total amount of pokemon, can also ask for amount of \"move\"s")
    print("\nType \"help\" to see this menu again")
    print("\nType \"quit\" to exit the program")


# Parsing
def parser(input_string,c):
    # Variables for data
    has_foreign = False
    columns = []
    identifiers = []
    # Bool to keep track of columns table
    pokemon_table = False

    # Check if it's "help"
    if input_string.lower() == "help":
        print_help()
        return None, None, None, None

    # Separate the data
    query_list = sep_string(input_string.lower())

    # Check if it's "load data"
    if len(query_list) == 2 and query_list[0] == "load" and query_list[1] == "data":
        # Call load data function
        load_data()
        print("Data Successfully Loaded")
        return None, None, None, None

    # Check if it's "how many pokemon/move"
    if len(query_list) == 3 and query_list[0] == "how" and query_list[1] == "many" and (
            query_list[2] == "pokemon" or query_list[2] == "move"):
        # Call how many function
        columns.append(query_list[2])
        return "COUNT", columns, None, None

    # Query Input Validation:
    # Ensure the length is odd since each column specified must have an identifier
    if len(query_list) % 2 != 1:
        print("Error, improper query format. Target must be specified and each column must have a corresponding identifier, enter \"help\" for more info")
        return None, None, None, None

    # Ensure the column names are valid, sort columns and identifiers into their lists
    for i in range(len(query_list)):
        # Column names are at odd indices since starting at 0
        if i % 2 == 1:
            if query_list[i] not in pokemon_columns and query_list[i] not in move_columns:
                print("Error, one or more column names were not valid, enter \"help\" for more info")
                return None, None, None, None
            # Otherwise it's valid
            else:
                columns.append(query_list[i])
        # If it's even (identifier)
        elif i % 2 == 0:
            # If it's the first term and not a valid column
            if i == 0 and (query_list[i] not in pokemon_columns and query_list[i] not in move_columns):
                print("Error, invalid target column, enter \"help\" for more info")
                return None, None, None, None
            # If it's not the first term
            elif i != 0:
                identifiers.append(query_list[i])

    # Determine if it uses a foreign key, move is in both tables so if it's first it will never have a foreign key
    if (query_list[0] in pokemon_columns and query_list[1] not in pokemon_columns and query_list[0] != "move") or \
            (query_list[0] in move_columns and query_list[1] not in move_columns and query_list[0] != "move"):
        has_foreign = True

    # Ensure that the identifiers are not empty
    for identifier in identifiers:
        if identifier == "":
            print("Error, at least one identifier was empty, enter \"help\" for more info")
            return None, None, None, None

    # Determine which table the identifying columns are referring to
    if columns[0] in pokemon_columns:
        pokemon_table = True
    # Iterate through the columns for additional checks
    for column in columns:
        # Make sure all of the identifying columns are from the same table
        if (pokemon_table and column not in pokemon_columns) or (not pokemon_table and column not in move_columns):
            print("Error, not all identifying columns from same table, enter \"help\" for more info")
            return None, None, None, None
        # Make sure there are no duplicate identifying columns
        if columns.count(column) > 1:
            print("Error, query contains duplicate identifying columns, enter \"help\" for more info")
            return None, None, None, None
        # Make sure none of the identifying columns match the target column (redundant query)
        if column == query_list[0]:
            print("Error, an identifying column matches the target column, enter \"help\" for more info")
            return None, None, None, None

    # Return the information
    return query_list[0], columns, identifiers, has_foreign



"""
    Method splits string into a list:
    it seperates the string in one of 2 ways:
    either at every space, or it will ignore spaces if a section is quoted
"""
def sep_string(in_string):
    #Initialize vars; query list will hold a list of commands
    query_list = []
    #Each command will be stored and appended to query list once finished
    command = ""
    #quoted_command and ignore_space are flags to allow the program to deal with a command in quotes
    #quoted_command sets flag for spaces in the middle of quotes
    quoted_command = False
    #ignore_space sets flag for space following closing quote
    ignore_space = False


    for ch in in_string:
        #if the ch is a space, end of command reached (unless within quotes), a$
        if ch == " " and not quoted_command:
            #if space following quote, it should be ignored
            if ignore_space:
                ignore_space = False
            else:
                query_list.append(command)
                command = ""

        #if ch is a quote, either start or end of command
        elif ch == '"' or ch == "'":
            #if end of command, add command to query list
            if quoted_command:
                quoted_command = False
                query_list.append(command)
                command = ""
                ignore_space = True

            #if start of command, set flag so that function will know if quote $
            else:
                quoted_command = True
        else:
            command += ch
    #at this point function adds command if last command is in quotes
    #if ignor space variable is false, the last command still needs to be added
    if not ignore_space:
        query_list.append(command)

    return query_list

# Translate to SQL query
def database_search(select_var,where_var,identifier, has_foreign, c,poke_table,move_table):


    # determine which table to start in where_var[1] is in either pokemon columns or move columns
    if (where_var[0] == "move"):
        if (select_var in pokemon_columns):
            from_table = poke_table
            other_table = move_table
        else:
            from_table = move_table
            other_table = poke_table
    else:
        if (where_var[0] in pokemon_columns):
            from_table = poke_table
            other_table = move_table
        else:
            from_table = move_table
            other_table = poke_table


    # determine number of AND statements
    and_count = len(where_var)-1
    and_list = []
    for i in range(and_count):
        and_list.append("AND")

    if(select_var == "pokemon"):
        select_var = "name"
    elif (select_var == "ptype" or select_var == "mtype"):
        select_var = "type"
    for item in range(len(where_var)):
        if (where_var[item] == "ptype" or where_var[item]  == "mtype"):
            where_var[item] = "type"
        if (where_var[item] == "pokemon"):
            where_var[item] = "name"

    from_table.replace("""""", "'")
    # create sql statement
    if (select_var == "COUNT"):
        sql_statement = 'SELECT COUNT(' + where_var[0] + ') FROM ' + from_table
    elif(has_foreign != True):
        sql_statement = 'SELECT DISTINCT ' + select_var + ' FROM ' + from_table + ' WHERE '
        for x in range(len(where_var)):
            sql_statement += where_var[x] + "="
            sql_statement += "'" + identifier[x]+ "'"
            if and_count > 0:
                sql_statement += " " + and_list[0] + " "
                and_list.pop()
                and_count -= 1
    else:
        sql_statement = 'SELECT DISTINCT ' + other_table + "." + select_var + ' FROM ' + other_table + ' INNER JOIN ' + \
                        from_table + ' ON ' + other_table + ".move" + "=" + from_table + ".move" + ' WHERE '

        for x in range(len(where_var)):
            sql_statement += from_table + "." + where_var[x] + "="
            sql_statement += "'" + identifier[x]+ "'"
            if and_count > 0:
                sql_statement += " " + and_list[0] + " "
                and_list.pop()
                and_count -= 1

    return c.execute(sql_statement).fetchall()


def main():
    print_help()

    c, conn, pokemon_table, move_table,exists = load_data()
    if(not exists):
        load_input = input("Please type 'load data' to start: ")
        while(load_input != 'load data'):
            print("Sorry you must type load data to start the program\n")
            load_input = input("Please type 'load data' to start: ")

    query_input = input("Please type your first query: ")
    while(query_input != "quit"):
        select_var, where_var, identifier, has_foreign = parser(query_input,c)
        # Ensure the query was successful
        if select_var is not None and where_var is not None:
            queried_data = database_search(select_var, where_var, identifier, has_foreign, c, pokemon_table, move_table)
            for x in queried_data:
                print(x[0])
        query_input = input("Next Query: ")
    conn.close()


def load_data():

    # Add exception handling
    conn = sqlite3.connect('test.db')
    c = conn.cursor()

    #if table exists
    #return c conn pokemon
    #if table not exists
    #create table

    c.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='pokemonData' ''')
    if c.fetchone()[0]==1 :
        pokemon_tosql = "pokemonData"
        move_tosql = "moveData"
        exists = True
        return c, conn, pokemon_tosql,move_tosql,exists
    else:
        c.execute('''DROP TABLE IF EXISTS test1''')
        c.execute('''DROP TABLE IF EXISTS test2''')
        c.execute('''CREATE TABLE IF NOT EXISTS test1 (pokemon text, ptype text, attack text, defense text, speed text, move text)''')
        c.execute('''CREATE TABLE IF NOT EXISTS test2 (move text, mtype text, category text, power text, accuracy text)''')

        pokemon_csv = "pokemon-data.csv"
        move_csv = "move-data.csv"
        # Add exception handling
        pokemon_tosql = "pokemonData"
        pokemon_df = pd.read_csv('../squirtle-warmup/' + pokemon_csv)
        pokemon_df.to_sql(pokemon_tosql, conn, if_exists='append', index=False)

        move_tosql = "moveData"
        move_df = pd.read_csv('../squirtle-warmup/' + move_csv)
        move_df.to_sql(move_tosql, conn, if_exists='append', index=False)
        exists = False
        return c, conn, pokemon_tosql, move_tosql,exists


main()
